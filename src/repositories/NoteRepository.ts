import { EntityRepository, Repository } from 'typeorm';
import { Note } from '../entity/Note';

@EntityRepository(Note)
export class NoteRepository extends Repository<Note> {

    readonly DEFAULT_ORDER = 'DESC';

    findAndPaginate(take: number, skip: number) {
        return this.findAndCount({
            order: { id: this.DEFAULT_ORDER },
            take: take,
            skip: skip
        });
    }
}