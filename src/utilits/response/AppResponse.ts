import { Request, Response } from 'express';
import { JsonpResponse } from './formats/JsonpResponse';

export class AppResponse {

    private formats = new Map<string, any>([
        [JsonpResponse.FORMAT, JsonpResponse]
    ]);

    private req: Request;
    private res: Response;

    constructor(req: Request, res: Response) {
        this.req = req;
        this.res = res;
    }

    public generate(body: any): Response {
        const format = this.req.query.format as string;

        if (format !== undefined) {
            const responder = this.formats.get(format);
            if (responder !== undefined) {
                return new responder(this.req, this.res).response(body);
            }
            return this.res.status(422).send({
                'code': 422,
                'message': 'Unsupported format'
            });
        }

        return this.res.json(body);
    }
}
