import { Request, Response } from 'express';
import { FormatResponseInterface } from '../FormatResponseInterface';

export class JsonpResponse implements FormatResponseInterface
{
    public static readonly FORMAT = 'jsonp';

    private req: Request;
    private res: Response;

    constructor(req: Request, res: Response) {
        this.req = req;
        this.res = res;
    }

    public response(body: any): Response {
        return this.res.jsonp(body);
    }
}
