import { Request, Response } from 'express';

export interface FormatResponseInterface
{
    response(req: Request, res: Response, body: any): Response;
}
