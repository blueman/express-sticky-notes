import { Request } from 'express';
import { validationResult } from 'express-validator';
import { HttpValidationModel } from './HttpValidationModel';

export class HttpValidator
{
    private readonly req: Request;

    constructor(req: Request) {
        this.req = req;
    }

    public validate(): HttpValidationModel {
        const validatorErrors = validationResult(this.req);
        let maxErrorCode = 0;
        let errors = [];
        if (!validatorErrors.isEmpty()) {
            validatorErrors.array().forEach((el) => {
                maxErrorCode = Math.max(maxErrorCode, el.msg.code || 0);
                errors.push(el.msg);
            });
        }

        return {
            code: maxErrorCode,
            errors: errors
        };
    }
}
