export type HttpValidationModel = {
    code: number;
    errors: any;
}
