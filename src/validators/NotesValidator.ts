import { check } from 'express-validator';
import { getRepository } from 'typeorm';
import { Note } from '../entity/Note';

let elementExists = (id) => {
    return getRepository(Note).findOneOrFail(id).catch(error => {
        return Promise.reject({'message': 'Element does not exist', 'code': 404});
    });
}

const titleMinLength = 3;

export default {
    createNote: [
        check('title')
            .isLength({ min:titleMinLength }).withMessage({'message': 'Title field is required', 'code': 400}),
        check('message')
            .exists().withMessage({'message': 'Message field is required', 'code': 400}),
    ],

    getNote: [
        check('id').custom(elementExists),
    ],

    updateNote: [
        check('id').custom(elementExists),
        check('title')
            .isLength({ min:titleMinLength }).withMessage({'message': 'Title field is required', 'code': 400}),
        check('message')
            .exists().withMessage({'message': 'Message field is required', 'code': 400}),
    ]
};
