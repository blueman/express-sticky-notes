import { Request, Response } from 'express';
import { AppResponse } from '../utilits/response/AppResponse';
import { NoteRepository } from '../repositories/NoteRepository';
import { HttpValidator } from '../utilits/validation/HttpValidator';
import { HttpValidationModel } from '../utilits/validation/HttpValidationModel';
import { Note } from '../entity/Note';
import { getCustomRepository, getManager } from 'typeorm';
import { _ as us } from 'underscore';

export class NotesController
{
    public async index(req: Request, res: Response) {
        const take: number = parseInt(req.query.take as string || '10');
        const skip: number = parseInt(req.query.skip as string || '0');

        const noteRepository = getCustomRepository(NoteRepository);
        const [notes, total] = await noteRepository.findAndPaginate(take, skip);

        return new AppResponse(req, res).generate({
            count: total,
            results: notes,
        });
    }

    public async create(req: Request, res: Response) {
        const validation: HttpValidationModel = (new HttpValidator(req)).validate();
        if (validation.errors.length > 0) {
            return res.status(validation.code).json({errors: validation.errors});
        }
        const requestBody = us.pick(req.body, ['title', 'message']);

        const manager = getManager();
        const newNote = manager.create(Note, requestBody);
        const results = await manager.save(newNote);

        return new AppResponse(req, res).generate(results);
    }

    public async update(req: Request, res: Response) {
        const validation: HttpValidationModel = (new HttpValidator(req)).validate();
        if (validation.errors.length > 0) {
            return res.status(validation.code).json({errors: validation.errors});
        }
        const requestBody = us.pick(req.body, ['title', 'message']);

        const noteRepository = getCustomRepository(NoteRepository);
        const note = await noteRepository.findOne(req.params.id);
        noteRepository.merge(note, requestBody);
        const results = await noteRepository.save(note);

        return new AppResponse(req, res).generate(results);
    }

    public async delete(req: Request, res: Response) {
        const validation: HttpValidationModel = (new HttpValidator(req)).validate();
        if (validation.errors.length > 0) {
            return res.status(validation.code).json({errors: validation.errors});
        }

        const noteRepository = getCustomRepository(NoteRepository);
        await noteRepository.delete(req.params.id);

        return new AppResponse(req, res).generate({success: true});
    }

    public async show(req: Request, res: Response) {
        const validation: HttpValidationModel = (new HttpValidator(req)).validate();
        if (validation.errors.length > 0) {
            return res.status(validation.code).json({errors: validation.errors});
        }

        const noteRepository = getCustomRepository(NoteRepository);
        const results = await noteRepository.findOne(req.params.id);

        return new AppResponse(req, res).generate(results);
    }
}
