import { AppResponse } from '../utilits/response/AppResponse';

export class PingController
{
    public index(req, res) {
        return new AppResponse(req, res).generate({'pong': true});
    }
}
