# Express(js) Sticky Notes
Small example usage of ExpressJS to create simple sticky notes with title and message.

## Usage
`npm run start`
By default the project is using port `3000`, but it is configured in `config/config.ts` file.

### Development
For a development you can use a command `npm run start-dev` which will watch for changes in file and restart a server.
In `data/` directory there are two Postman files to import collection and environment variables.
