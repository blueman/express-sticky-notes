import { PingController } from "../src/controllers/PingController";
import { getMockReq, getMockRes } from '@jest-mock/express'

describe('Ping', () => {
    describe('/GET ping', () => {
        it('it should GET pong response', () => {
            const mockRequest = getMockReq();
            const mockResponse = {
                json: (data: any) => data,
            };

            const resp = (new PingController()).index(mockRequest, mockResponse);
            expect(resp).toEqual({ pong: true });
        });

        it('it should GET unknown format response error', () => {
            const mockRequest = getMockReq({
                query: {
                    format: 'unknown'
                }
            });
            const mockResponse = {
                status: () => ({
                    send: (data) => data,
                }),
                json: (data) => data,
            };

            const resp = (new PingController()).index(mockRequest, mockResponse);
            expect(resp).toBeInstanceOf(Object);
            expect(resp).toHaveProperty('code');
            expect(resp).toHaveProperty('message');
        });

        it('it should GET pong jsonp response', () => {
            const mockRequest = getMockReq({
                query: {
                    format: 'jsonp'
                }
            });
            const mockResponse = {
                status: () => ({
                    send: (data) => data,
                }),
                jsonp: (data) => data,
            };

            const resp = (new PingController()).index(mockRequest, mockResponse);
            expect(resp).toBeInstanceOf(Object);
        });
    });
});
