import * as express from 'express';
import * as cors from 'cors';
import config from './config/config';
import { indexRoutes } from './config/routes';
import * as createError from 'http-errors';
import { createConnection } from 'typeorm';

// create and setup express app
const app = express();
app.use(cors());
app.use(express.json());
app.use((req, res, next) => {
    res.setHeader('Cache-Control', 'no-cache');
    next();
});
app.use(express.urlencoded({extended: false}));

createConnection().then(connection => {
    // routes
    app.use('/v1', indexRoutes);

    app.use((req, res, next) => {
        next(createError(404));
    });

    app.use((err, req, res, next) => {
        res.locals.message = err.message;
        res.status(err.status || 500);
        res.send(err);
    });
});

// start express server
app.listen(config.port, () => {
    console.log('Server started on ports ' + config.port);
});

module.exports = app;