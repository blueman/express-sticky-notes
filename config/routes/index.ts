import { Router } from 'express';
import pingRoutes from './ping';
import notesRoutes from './notes';

const router: Router = Router();

router.use('/ping', pingRoutes);
router.use('/notes', notesRoutes);

export const indexRoutes: Router = router;
