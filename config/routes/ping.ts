import { Router } from 'express';
import { PingController } from '../../src/controllers/PingController';

const router: Router = Router();
const controller: PingController = new PingController();

router.get('/', controller.index);

export default router;
