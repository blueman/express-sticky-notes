import { Router } from 'express';
import { NotesController } from '../../src/controllers/NotesController';
import v from '../../src/validators/NotesValidator';

const router: Router = Router();
const controller: NotesController = new NotesController();

router.post('/', v.createNote, controller.create);
router.get('/', controller.index);
router.put('/:id(\\d+)', v.updateNote, controller.update);
router.get('/:id(\\d+)', v.getNote, controller.show);
router.delete('/:id(\\d+)', v.getNote, controller.delete);

export default router;
